const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'sekawanapprove'
    },
    port: process.env.PORT || 5001,
    db: 'mongodb://localhost/sekawanapprove-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'sekawanapprove'
    },
    port: process.env.PORT || 5001,
    db: 'mongodb://localhost/sekawanapprove-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'sekawanapprove'
    },
    port: process.env.PORT || 5001,
    db: 'mongodb://localhost/sekawanapprove-production'
  }
};

module.exports = config[env];
