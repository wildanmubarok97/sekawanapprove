let user = localStorage.getItem("tokensekawanapprove");
$('#closeDetail').on('click' , function(e){
    $('#ModalConfirm').css("display" , "none");
    location.reload();
})

$.ajax({
    type    :   "POST",
    url     :   IP+"/v1/rent/listbyatasan",
    headers :   {
        "Authorization" :   user
    },
    success:function(datas){
        if(datas.hasil   ==  true){
            let data = datas.result;
            $.each(data, function(i, item){
                var show    =   '<tr>'  +
                                    '<td>'  +   (i + 1)             +   '</td>' +
                                    '<td>'  +   data[i].Mobil       +   '</td>' +
                                    '<td>'  +   data[i].Driver      +   '</td>' +
                                    '<td>'  +   data[i].Ambil       +   '</td>' +
                                    '<td>'  +   data[i].Kembalikan  +   '</td>' +
                                    '<td>'  +
                                        '<button onclick="ButtonConfirm('       +   "'"     +   data[i].Kode    +   "'" +   ')" class="btn btn-outline-success">Confirm</button>' +
                                    '</td>' +
                                '</tr>'
                $("#tableConfirm").append(show);
            })
        }
    }
})

function ButtonConfirm(kode){
    $.ajax({
        type    :   "POST",
        url     :   IP+"/v1/rent/update",
        data    :   {
            Kode    :   kode
        },
        headers :   {
            "Authorization" :   user
        },
        success:function(datas){
            $('#ModalConfirm').css("display" , "block");
            $("#message").html(datas.message);
        }
    })
}