const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Article = mongoose.model('Article');

module.exports = (app) => {
  app.use('/', router);
};

// router.get('/', (req, res, next) => {
//   Article.find((err, articles) => {
//     if (err) return next(err);
//     res.render('index', {
//       title: 'Generator-Express MVC',
//       articles: articles
//     });
//   });
// });

router.get("/", function (req, res, next) {
  res.render("home", {
    title: "Home",
    layout: "home",
  });
});

router.get("/history", function (req, res, next) {
  res.render("history", {
    title: "History",
    layout: "home",
  });
});

router.get("/login", function (req, res, next) {
  res.render("login", {
    title: "Login",
    layout: "login",
  });
});